package main

import (
	"fmt"
	"math"
)

func FastPow(cur, pow, k_mod int64) int64 {
	cur %= k_mod
	if pow == 0 {
		return 1
	}
	if pow%2 == 0 {
		return FastPow(cur*cur, pow/2, k_mod)
	}
	return (cur * FastPow(cur, pow-1, k_mod)) % k_mod
}

func main() {
	vals := make(map[int]int)
	var prime, base, res int64
	for {
		_, err := fmt.Scan(&prime, &base, &res)
		for k := range vals {
			delete(vals, k)
		}
		if err != nil {
			break
		}
		sqrtPrime := int(math.Sqrt(math.Sqrt(float64(prime))))
		aR := int64(1)
		for i := 0; i < sqrtPrime; i++ {
			if _, ok := vals[int(aR)]; !ok {
				vals[int(aR)] = i
			}
			aR = (aR * base) % prime
		}
		reverseALimit := FastPow(base, prime-2, prime)
		reverseALimit = FastPow(reverseALimit, int64(sqrtPrime), prime)
		currentLimitReverse := int64(1)
		notFound := true
		power := prime
		coefSqrt := int64(0)
		for coefSqrt*int64(sqrtPrime) <= prime {
			cur := (res * currentLimitReverse) % prime
			if _, ok := vals[int(cur)]; !ok {
				currentLimitReverse *= reverseALimit
				currentLimitReverse %= prime
			} else {
				notFound = false
				power = int64(math.Min(float64(power), float64(vals[int(cur)])+float64(coefSqrt)*float64(sqrtPrime)))
			}
			coefSqrt++
		}
		if notFound {
			fmt.Println("no solution")
		} else {
			fmt.Println(power % (prime - 1))
		}
	}
}
