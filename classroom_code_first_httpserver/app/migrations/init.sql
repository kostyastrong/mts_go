CREATE TABLE IF NOT EXISTS todos
(
    id         INTEGER PRIMARY KEY AUTOINCREMENT,
    task       TEXT      NOT NULL,
    comment    TEXT      NULL,
    is_done    BOOL      NOT NULL,
    created_at TIMESTAMP NOT NULL,
    deleted_at TIMESTAMP NULL
);

CREATE INDEX IF NOT EXISTS TASK_IDX ON TODOS (TASK);