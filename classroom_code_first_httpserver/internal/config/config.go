package config

import (
	"os"

	"gopkg.in/yaml.v2"
)

type Config struct {
	Http Http `yaml:"http"`
	DB   DB   `yaml:"db"`
}

type Http struct {
	Port int `yaml:"port"`
}

type DB struct {
	FilePath string `yaml:"filePath"`
}

func NewConfig(filePath string) (*Config, error) {
	data, err := os.ReadFile(filePath)
	if err != nil {
		return nil, err
	}

	var cfg Config
	err = yaml.Unmarshal(data, &cfg)
	if err != nil {
		return nil, err
	}

	return &cfg, nil
}
