package storage

var (
	ErrNoElements    = &StorageError{Message: "no elements"}
	ErrNotFound      = &StorageError{Message: "not found"}
	ErrAlreadyExists = &StorageError{Message: "already exists"}
)

type StorageError struct {
	Message string
}

func (se *StorageError) Error() string {
	return se.Message
}
