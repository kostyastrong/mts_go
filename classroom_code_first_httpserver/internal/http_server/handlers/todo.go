package handlers

import (
	"encoding/json"
	"net/http"

	"Todo/internal/services"
	"Todo/models"
)

type Todo struct {
	s *services.Todo
}

func NewTodoHandlers(service *services.Todo) *Todo {
	return &Todo{
		s: service,
	}
}

func (t *Todo) GetHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	todos, err := t.s.GetAll(ctx)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp, _ := json.Marshal(todos)

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	_, _ = w.Write(resp)
	return
}

func (t *Todo) CreateHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	todo := &models.Todo{}

	dec := json.NewDecoder(r.Body)
	err := dec.Decode(todo)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = t.s.Create(ctx, todo)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		_, _ = w.Write([]byte(err.Error()))
		return
	}

	w.WriteHeader(http.StatusCreated)
	return
}
