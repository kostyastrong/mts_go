package authsvc_test

import (
	"context"
	"hw4/internal/model"
	"hw4/internal/repo/repomock"
	"hw4/internal/service"
	"hw4/internal/service/authsvc"

	"testing"
	"time"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

func TestTokens(t *testing.T) {
	ctx := context.Background()

	userRepo := repomock.NewUser()
	userRepo.On(
		"WithNewTx",
		mock.MatchedBy(func(ctx context.Context) bool { return true }),
		mock.MatchedBy(func(f func(ctx context.Context) error) bool { return true }),
	).Return(&model.User{Login: "login"}, nil)
	userRepo.On(
		"ValidateUser",
		mock.MatchedBy(func(ctx context.Context) bool { return true }),
		"login",
		"password",
	).Return(&model.User{Login: "login"}, nil)

	config := &service.AuthConfig{
		SigningKey:           "signingKey",
		AccessTokenDuration:  1 * time.Second,
		RefreshTokenDuration: 2 * time.Second,
	}

	svc := authsvc.New(config, userRepo)

	initialPair, err := svc.Login(ctx, "login", "password")
	require.Nil(t, err)

	newPair, err := svc.ValidateAndRefresh(ctx, initialPair)
	require.Nil(t, err)

	require.Equal(t, initialPair.AccessToken, newPair.AccessToken)
	require.Equal(t, initialPair.RefreshToken, newPair.RefreshToken)

	time.Sleep(config.AccessTokenDuration)

	newPair, err = svc.ValidateAndRefresh(ctx, initialPair)
	require.Nil(t, err)

	require.NotEqual(t, initialPair.AccessToken, newPair.AccessToken)
	require.NotEqual(t, initialPair.RefreshToken, newPair.RefreshToken)

	time.Sleep(config.RefreshTokenDuration)

	_, err = svc.ValidateAndRefresh(ctx, newPair)
	require.ErrorIs(t, err, service.ErrForbidden)
}
