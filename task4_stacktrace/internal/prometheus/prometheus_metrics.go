package prometheus

import "github.com/prometheus/client_golang/prometheus"

var (
	RequestsTotal = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "auth_requests_total",
		Help: "Total number of HTTP requests received",
	})

	ResponseTime = prometheus.NewSummary(prometheus.SummaryOpts{
		Name: "auth_response_time",
		Help: "Response time of HTTP requests",
	})

	CurrentActiveUsers = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "auth_current_active_users",
		Help: "Number of active users",
	})

	RequestDuration = prometheus.NewHistogram(prometheus.HistogramOpts{
		Name:    "auth_request_duration_seconds",
		Help:    "HTTP request duration in seconds",
		Buckets: []float64{0.1, 0.2, 0.5, 1, 2, 5, 10}, // buckets
	})
)

func Init() {
	prometheus.MustRegister(RequestsTotal)
	prometheus.MustRegister(ResponseTime)
	prometheus.MustRegister(CurrentActiveUsers)
	prometheus.MustRegister(RequestDuration)
	CurrentActiveUsers.Set(0)
}
