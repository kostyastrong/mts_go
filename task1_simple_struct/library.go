package main

import (
	"errors"
	"fmt"
)

type Storage interface {
	GetBook(id int32) (Book, error)
	AddBook(book Book, id int32)
}

type MapStorage struct {
	books map[int32]Book
}

func (storage *MapStorage) GetBook(id int32) (Book, error) {
	book, ok := storage.books[id]
	if ok {
		return book, nil
	}
	return Book{}, NoBookError(id)
}

func (storage *MapStorage) AddBook(book Book, id int32) {
	storage.books[id] = book
}

type BookAndId struct {
	id   int32
	book Book
}

type SliceStorage struct {
	books []BookAndId
}

func (storage *SliceStorage) AddBook(book Book, id int32) {
	storage.books = append(storage.books, BookAndId{id, book})
}

func (storage *SliceStorage) GetBook(id int32) (Book, error) {
	for _, bookAndId := range storage.books {
		if bookAndId.id == id {
			return bookAndId.book, nil
		}
	}
	return Book{}, NoBookError(id)
}

func NoBookError(id int32) error {
	message := fmt.Sprintf("No book with %d id found", id)
	return errors.New(message)
}

type Library struct { // library is different from storage, because storage doesn't know about hashes
	storage Storage
	genId   func(string) int32
}

func (lib *Library) AddBook(book Book) { // user of library doesn't know about genId func
	lib.storage.AddBook(book, lib.genId(book.title))
}

func (lib *Library) GetBook(title string) (Book, error) { // because addBook is necessary to add
	return lib.storage.GetBook(lib.genId(title))
}

func (lib *Library) NewMapStorage() {
	lib.storage = &MapStorage{books: make(map[int32]Book)} // previous storage will be cleared by collector
}

func (lib *Library) NewSliceStorage() {
	lib.storage = &SliceStorage{books: make([]BookAndId, 0)} // previous storage will be cleared by collector
}

type Book struct {
	title  string
	text   string
	author string
}
