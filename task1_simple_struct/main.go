package main

import (
	"fmt"
	"log"
	"strconv"
	// https://habr.com/ru/companies/joom/articles/666440/
)

func Hash(name string, base int32) int32 {
	var hash int32 = 0
	var MOD int32 = 1e7 + 7
	if base < 256 {
		panic("base for hash is less than ASCII")
	}

	for _, char := range name {
		hash *= base
		hash += char
		hash %= MOD
	}
	return hash
}

func Hash281(name string) int32 { // first gen id function
	return Hash(name, 281)
}

func Hash283(name string) int32 { // another gen id function
	return Hash(name, 283)
}

func main() {
	library := Library{genId: Hash281}
	library.NewMapStorage()

	for i := 0; i < 6; i++ {
		library.AddBook(Book{strconv.Itoa(i), fmt.Sprintf("title%d", i), "Vasya Pupkin"})
	}

	log.Println("-------------")
	log.Println("Trying to get books 1 and 2:")
	log.Println("Expected:\n\t {1 title1 Vasya Pupkin}")
	book, _ := library.GetBook("1")
	log.Print("Actual:\n\t", book)
	log.Println("-------------")
	book, _ = library.GetBook("2")
	log.Println("Expected:\n\t {2 title2 Vasya Pupkin}")
	log.Print("Actual:\n\t", book)
	log.Println("-------------")

	library.NewSliceStorage()
	library.genId = Hash283

	for i := 0; i < 6; i++ {
		library.AddBook(Book{strconv.Itoa(i), fmt.Sprintf("title%d", i), "Vasya Pupkin"})
	}
	log.Println("Trying to get books 4 and 5 from new storage:")
	log.Println("Expected:\n\t {4 title4 Vasya Pupkin}")
	book, _ = library.GetBook("4")
	log.Print("Actual:\n\t", book)
	log.Println("-------------")
	log.Println("Expected:\n\t {5 title5 Vasya Pupkin}")
	book, _ = library.GetBook("5")
	log.Print("Actual:\n\t", book)
	log.Println("-------------")

	log.Println("Get 6th book:")
	log.Println("Expected: No book with 54 id found") // hash of "6" in 283 is 54
	_, err := library.GetBook("6")
	log.Println("Actual:", err)
}
