package main

import (
	"fmt"
	"reflect"
	"strconv"
	"testing"
)

func TestLibrary_GetBook(t *testing.T) {
	var tests = []struct {
		name  string
		title string
		want  Book
		err   error
	}{
		{name: "get 1st book", title: "1", want: Book{"1", "title1", "Vasya Pupkin"}, err: nil},
		{name: "get 2nd book", title: "2", want: Book{"2", "title2", "Vasya Pupkin"}, err: nil},
		{name: "6th book should be empty", title: "6", want: Book{}, err: NoBookError(Hash283("6"))},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			lib := &Library{storage: &SliceStorage{books: make([]BookAndId, 0)}, genId: Hash283}

			for i := 0; i < 6; i++ {
				lib.AddBook(Book{strconv.Itoa(i), fmt.Sprintf("title%d", i), "Vasya Pupkin"})
			}
			if got, err := lib.GetBook(tt.title); !reflect.DeepEqual(got, tt.want) || !reflect.DeepEqual(err, tt.err) {
				t.Errorf("GetBook() = %v, want %v", err, tt.err)
			}
		})
	}
}
