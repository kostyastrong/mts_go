The task:
_________

Describe the structure and methods of the book at your own discretion. Write a library with a book storage based on
slice and map. The library should be able to give out books by name, while the repository should be able to give out
books by id. (library clients do not know how book storage is organized).

When a library adds a book to it, it must assign an identifier to the book (using the identifier generator function)
Describe the structure of the book (with fields: title, author, etc. and methods for obtaining them) Describe the
structure of the storage (the storage can be on a map or on a slice) Describe library structure with storage.
___
**The _library_ should be able to**:

* search for a book by name (translate the name into id using a given function and search inside the repository)
* add a book to storage
* when adding the book id should be generated using a function based on the book title
___
It is necessary to demonstrate the scenario: Initialize a library with some storage and fill the library with 5
different books. Find two different books in the storage Next, we change the storage type and the identifier generator
function to another and demonstrate the same scenario. This made directly in main.go

To launch main.go: type ```go run main.go library.go``` in terminal
