package main

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"time"
)

type App struct {
	server  *http.Server
	baseURL string
}

func (app *App) SetUrl(url string) {
	app.baseURL = url
}

func (app *App) Run() {
	log.Println("Starting app")
	http.HandleFunc("/hard-op", getHardOp)
	http.HandleFunc("/version", getVersion)
	http.HandleFunc("/decode", postDecode)

	go func() {
		err := http.ListenAndServe(":8080", nil)
		if err != nil {
			fmt.Println(err)
		}
	}()
	log.Printf("App started on port 8080\n")
}

func (app *App) Stop(ctx context.Context) {
	log.Printf("Stopping app\n")
	err := app.server.Shutdown(ctx)
	if err != nil {
		log.Println("Error happened when shutting down app")
		return
	}
	log.Printf("App stopped\n")
}

func getHardOp(w http.ResponseWriter, r *http.Request) {
	log.Printf("got /hard-op request\n")
	sleepDuration := time.Duration(rand.Intn(11)+10) * time.Second
	time.Sleep(sleepDuration)

	// Generate a random response status (500 or http.StatusOK)
	responseStatus := http.StatusOK
	if rand.Intn(2) == 0 {
		responseStatus = http.StatusInternalServerError
	}

	// Send the response
	w.WriteHeader(responseStatus)
	if responseStatus == http.StatusOK {
		w.Write([]byte("OK"))
	} else {
		w.Write([]byte("Error"))
	}
}

func getVersion(w http.ResponseWriter, r *http.Request) {
	log.Printf("got /version request\n")
	apiVersion := "1.0.0"
	// Send the response
	w.WriteHeader(http.StatusOK)
	_, err := w.Write([]byte(apiVersion))
	if err != nil {
		log.Fatalf("Error happened writing in response")
	}
}

func postDecode(w http.ResponseWriter, r *http.Request) {
	log.Println("got /decode request", r.Body)
	// Parse the request body
	var requestBody struct {
		InputString string `json:"inputString"`
	}
	err := json.NewDecoder(r.Body).Decode(&requestBody)
	decodedString, err := base64.StdEncoding.DecodeString(requestBody.InputString)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Create the response JSON
	response := struct {
		OutputString string `json:"outputString"`
	}{
		OutputString: string(decodedString),
	}
	log.Println("Decoded string: ", decodedString)

	// Send the response
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(response)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
