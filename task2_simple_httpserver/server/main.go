package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"time"
)

func main() {
	flagUrl := flag.String("baseUrl", "http://localhost:8080", "set baseUrl")
	flag.Parse()
	baseURL := *flagUrl

	server := &App{}
	server.SetUrl(baseURL)

	ctx, stop := context.WithTimeout(context.Background(), 1*time.Minute)

	defer stop()

	defer func() {
		v := recover()

		if v != nil {
			ctx, _ := context.WithTimeout(ctx, 3*time.Second)
			server.Stop(ctx)
			fmt.Println(v)
			os.Exit(1)
		}
	}()

	server.Run()
	<-ctx.Done()
	server.Stop(ctx)
}
