package main

import (
	"flag"
	"log"
	"net/http"
)

func main() {
	flagUrl := flag.String("baseUrl", "http://localhost:8080", "set baseUrl")
	flag.Parse()
	baseURL := *flagUrl
	statusCode := 0

	// make client
	client := &Client{}

	statusCode = client.sendVersionRequest(baseURL)
	if statusCode != http.StatusOK {
		log.Println("Version requestfailed. Exiting...")
		return
	}

	statusCode = client.sendDecodeRequest(baseURL)
	if statusCode != http.StatusOK {
		log.Println("Decode request failed. Exiting...")
		return
	}

	statusCode = client.sendHardOpRequest(baseURL)
	if statusCode != http.StatusOK {
		log.Printf("Hard-op request failed. Error code: %d. Exiting...", statusCode)
		return
	}

	log.Printf("Last request success: %t, Last request status code: %d\n", statusCode == http.StatusOK, statusCode)
}
