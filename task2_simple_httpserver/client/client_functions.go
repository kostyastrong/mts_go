package main

import (
	"bytes"
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"time"
)

type Client struct {
	urlServer string
}

func (c *Client) SetUrl(urlServer string) {
	c.urlServer = urlServer
}

func (c *Client) sendDecodeRequest(urlServer string) int {
	url := fmt.Sprintf("%s/decode", urlServer)
	encodedString := base64.StdEncoding.EncodeToString([]byte("Hello, server!"))
	// Create the request payload
	payload, err := json.Marshal(map[string]string{
		"inputString": encodedString,
	})
	if err != nil {
		log.Fatal(err)
	}

	resp, err := http.Post(url, "application/json", bytes.NewBuffer(payload))
	if err != nil {
		log.Fatal(err)
	}

	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Fatalf("Error happened reading response body: %s", err)
	}

	var response struct {
		OutputString string `json:"outputString"`
	}
	err = json.Unmarshal(body, &response)
	if err != nil {
		log.Fatalf("Error happened reading response body: %s", err)
	}

	fmt.Printf("Decoded string: %s", body)
	return resp.StatusCode // true, because request is completed
}

// sendTimeoutRequest sends a timeout request to the specified URL using the specified HTTP method.
//
// Parameters:
// - method: the HTTP method to use for the request.
// - url: the URL to send the request to.
// - timeout: the duration after which the request should time out.
//
// Returns:
// - a boolean indicating whether the request was successful.
// todo: should it be error instead?
// - an integer representing the status code of the response
func (c *Client) sendTimeoutRequest(method string, url string, timeout time.Duration) (string, int) {
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	req, err := http.NewRequest(method, url, nil)
	if err != nil {
		log.Fatal(err)
	}

	resp, err := http.DefaultClient.Do(req.WithContext(ctx))
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close() // todo: Is error handling needed here?

	body, err := io.ReadAll(resp.Body)
	// body is bytes.Buffer, so it's just any type, but []byte, then in log we will print it
	if err != nil {
		log.Fatalf("Error happened reading response body: %s", err)
	}
	return string(body), resp.StatusCode
}

func (c *Client) sendVersionRequest(urlServer string) int {
	url := fmt.Sprintf("%s/version", urlServer)
	body, status := c.sendWithoutTimeoutRequest("GET", url)
	fmt.Println(body)
	return status
}

func (c *Client) sendHardOpRequest(urlServer string) int {
	url := fmt.Sprintf("%s/hard-op", urlServer)
	body, status := c.sendTimeoutRequest("GET", url, 15*time.Second)
	fmt.Println(body, status)
	return status
}

func (c *Client) sendWithoutTimeoutRequest(method string, url string) (string, int) {
	return c.sendTimeoutRequest(method, url, 1*time.Minute) // time.Duration(0) means no timeout
}
