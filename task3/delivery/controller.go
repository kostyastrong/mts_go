package delivery

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"sync"
	"task3/domain"
)

type Controller struct {
	mutex             sync.RWMutex
	balanceRepository domain.BalanceRepository
}

func (c *Controller) CreditFunds(w http.ResponseWriter, r *http.Request) {
	userID := r.FormValue("user_id")
	amountStr := r.FormValue("amount")
	amount, err := strconv.ParseFloat(amountStr, 64)
	if err != nil {
		http.Error(w, "Invalid amount", http.StatusBadRequest)
		return
	}

	c.mutex.Lock()
	defer c.mutex.Unlock()

	balance, _ := c.balanceRepository.GetBalanceById(userID)

	balance.Amount += amount
	c.balanceRepository.SetBalanceById(userID, balance)

	w.WriteHeader(http.StatusOK)
}

func (c *Controller) TransferFunds(w http.ResponseWriter, r *http.Request) {
	senderID := r.FormValue("sender_id")
	recipientID := r.FormValue("recipient_id")
	amountStr := r.FormValue("amount")
	amount, err := strconv.ParseFloat(amountStr, 64)
	if err != nil {
		http.Error(w, "Invalid amount", http.StatusBadRequest)
		return
	}

	c.mutex.Lock()
	defer c.mutex.Unlock()

	if senderID == recipientID {
		http.Error(w, "Cannot transfer funds to oneself", http.StatusBadRequest)
		return
	}

	senderBalance, ok := c.balanceRepository.GetBalanceById(senderID)
	if !ok {
		http.Error(w, "Sender balance not found", http.StatusBadRequest)
		return
	}

	recipientBalance, ok := c.balanceRepository.GetBalanceById(recipientID)
	if !ok {
		http.Error(w, "Recipient balance not found", http.StatusBadRequest)
		return
	}

	if senderBalance.Amount < amount {
		http.Error(w, "Insufficient funds", http.StatusBadRequest)
		return
	}

	log.Println(amount, senderBalance, recipientBalance)
	senderBalance.Amount -= amount
	recipientBalance.Amount += amount

	c.balanceRepository.SetBalanceById(senderID, senderBalance)
	c.balanceRepository.SetBalanceById(recipientID, recipientBalance)
	log.Println("Balance updated: ", c.balanceRepository.GetAll())

	w.WriteHeader(http.StatusOK)
}

func (c *Controller) CheckBalance(w http.ResponseWriter, r *http.Request) {
	userID := r.FormValue("user_id")

	c.mutex.RLock()
	defer c.mutex.RUnlock()

	balance, ok := c.balanceRepository.GetBalanceById(userID)
	if !ok {
		http.Error(w, "Balance not found", http.StatusBadRequest)
		return
	}

	json.NewEncoder(w).Encode(balance)
}

func NewController() *Controller {
	return &Controller{
		balanceRepository: domain.NewBalanceList(),
		mutex:             sync.RWMutex{},
	}
}
