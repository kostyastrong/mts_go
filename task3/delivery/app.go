package delivery

import (
	"context"
	"fmt"
	"log"
	"net/http"
)

type App struct {
	url        string
	server     *http.Server
	controller *Controller
}

func NewApp(url string) *App {
	log.Printf("Creating app with url %s\n", url)
	return &App{
		controller: NewController(),
		url:        url,
		server:     &http.Server{Addr: ":8080"},
	}
}

func (app *App) Run() {
	log.Println("Starting app")
	http.HandleFunc("/credit", app.controller.CreditFunds)
	http.HandleFunc("/transfer", app.controller.TransferFunds)
	http.HandleFunc("/balance", app.controller.CheckBalance)

	go func() {
		err := app.server.ListenAndServe()
		if err != nil {
			fmt.Println(err)
		}
	}()
	log.Printf("App started on port 8080\n")
}

func (app *App) Stop(ctx context.Context) {
	log.Printf("Stopping app\n")
	err := app.server.Shutdown(ctx)
	if err != nil {
		log.Println("Error happened when shutting down app")
		return
	}
	log.Printf("App stopped\n")
}
