package domain

type BalanceRepository interface {
	GetBalanceById(userID string) (Balance, bool)
	SetBalanceById(userID string, balance Balance)
	GetAll() map[string]Balance
}

type BalanceList struct {
	Balances map[string]Balance
}

func (b *BalanceList) GetBalanceById(userID string) (Balance, bool) {
	balance, ok := b.Balances[userID]
	if !ok {
		balance = Balance{}
	}
	return balance, ok
}

func (s *BalanceList) SetBalanceById(userID string, balance Balance) {
	s.Balances[userID] = balance
}

func (s *BalanceList) GetAll() map[string]Balance {
	return s.Balances
}

func NewBalanceList() *BalanceList {
	return &BalanceList{
		Balances: make(map[string]Balance),
	}
}
