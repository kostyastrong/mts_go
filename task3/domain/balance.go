package domain

type Balance struct {
	Amount float64 `json:"amount"`
}
