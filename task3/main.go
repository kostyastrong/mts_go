package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"task3/delivery"
	"time"
)

func main() {
	flagUrl := flag.String("baseUrl", "http://localhost:8080", "set baseUrl")
	flag.Parse()
	baseURL := *flagUrl

	app := delivery.NewApp(baseURL)

	ctx, stop := context.WithTimeout(context.Background(), 10*time.Minute)

	defer stop()

	go func() {
		defer handleRecover(ctx)
	}()

	app.Run()
	<-ctx.Done()
	app.Stop(ctx)
}

func handleRecover(ctx context.Context) {
	if r := recover(); r != nil {
		fmt.Printf("recovered panic from %+v", r)
		os.Exit(1)
	}
}
